
package javaapplication9;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Terminal {
public Terminal(){
        
    }
    
    public String help(){
        String help = "help -> Lista los comandos con una breve definición de lo que hacen.\ncd-> Muestra el directorio actual.\n\t [..] -> Accede al directorio padre.\n\t [..] -> [<nombreDirectorio>] -> Accede a un directorio dentro del directorio\n" +"actual.\n\t [..] -> [<rutaAbsoluta] -> Accede a la ruta absoluta del sistema.\nmkdir <nombre_directorio> -> Crea un directorio en la ruta actual.\ninfo <nombre> -> Muestra la información del elemento. Indicando FileSystem,\n" +"Parent, Root, Nº of elements, FreeSpace, TotalSpace y UsableSpace.\ncat <nombreFichero> -> Muestra el contenido de un fichero.\ntop <numeroLineas> <nombreFichero> -> Muestra las líneas especificadas de\n" +"un fichero.\nmkfile <nombreFichero> <texto> -> Crea un fichero con ese nombre y el\n" +"contenido de texto.\nwrite <nombreFichero> <texto> -> Añade 'texto' al final del fichero\n" +"especificado.\ndir -> Lista los archivos o directorios de la ruta actual.\n" +" [<nombreDirectorio>] -> Lista los archivos o directorios dentro de ese\n" +"directorio.\ndelete <nombre> -> Borra el fichero, si es un directorio borra todo su\n" +"contenido y a si mismo.\nclose -> Cierra el programa.\nClear -> vacía la vista.";        
        return help;
    }   
    
    public boolean close(){
         return false;
    }
    
    public String clear(){
        String clear = "\n";        
        return clear;
    }
    
    public void mkdir(String ruta){
        File directorio = new File(ruta);
        
        if (!directorio.exists()) {
            if (directorio.mkdirs()) {
                System.out.println("Directorio creado");
            } else {
                System.out.println("Error al crear directorio");
            }
        }
    }
    
    public void mkfile(String ruta, String texto) throws IOException{
        File archivo = new File(ruta);
        BufferedWriter bw;
        if(archivo.exists()) {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(texto);
            System.out.println("Fichero escrito");
        } else {
            bw = new BufferedWriter(new FileWriter(archivo));
            bw.write(texto);
             System.out.println("Fichero escrito");
        }
        bw.close();
    }
    
    public void delete(String ruta){
        File fichero = new File(ruta);
        
        if (fichero.exists() && !fichero.isDirectory()) {
            if (fichero.delete()) {
                System.out.println("Fichero borrado");
            } else {
                System.out.println("Error al borrar fichero");
            }
        }else if (fichero.exists() && fichero.isDirectory()) {
            if (fichero.delete()) {
                System.out.println("Directorio borrado");
            } else {
                System.out.println("Error al borrar directorio");
            }
        }               
    }
    
    public void cd(String ruta){
        File direccion = new File(ruta);
        
        
        if(ruta.equals("..")){
            File fichero2 = new File(direccion.getAbsolutePath());		
            System.out.println(fichero2.getParent());
        }else if(direccion.isAbsolute()){
            System.out.println(direccion.getAbsolutePath());
        }else if (direccion.isDirectory()){
            String[] archivos = direccion.list();
            
            for (String archivo : archivos) {
                System.out.println(archivo);
            }
        }
        else{
        Path rutas = Paths.get(System.getProperty("user.dir"));
        System.out.println(rutas.toString());  
        }
    }
    
    public void dir(String ruta) {
        File direccion = new File(ruta);
        
        File directorio = new File(ruta);
            if(directorio.exists() && directorio.isDirectory()) {
		File[] lista = directorio.listFiles();
            for (File lista1 : lista) {
                System.out.println(lista1.getName());
            }
	}else {
            System.out.println("No existe o no es un directorio");
	}
    }
    
    public void info(){
        File direccion = new File("user.dir");
        FileSystem ficheros = FileSystems.getDefault();
        Path rutaFichero = ficheros.getPath("user.dir");
        
            //System.out.println(rutaFichero.getFileSystem());
            System.out.println(direccion.getFreeSpace());
            System.out.println(direccion.getTotalSpace());
            System.out.println(direccion.getUsableSpace());
            System.out.println(direccion.getUsableSpace());
            System.out.println(direccion.getAbsoluteFile());
            System.out.println(direccion.getParent());
            System.out.println(rutaFichero.getRoot());
            System.out.println(direccion.listFiles());
    }
    public void cat (String texto) throws IOException {

        String cadena;
        FileReader f = new FileReader(texto);
        try (BufferedReader b = new BufferedReader(f)) {
            while((cadena = b.readLine())!=null) {
                System.out.println(cadena);
            }
        }
    }
    
    public void top(String fichero) throws FileNotFoundException, IOException{
        try {
        FileReader fr = new FileReader(fichero);
        BufferedReader bf = new BufferedReader(fr);
        long NumeroLineas = 0;
        String sCadena;
       
        while ((sCadena = bf.readLine())!= null ) {
            NumeroLineas++;
        }
        System.out.println(NumeroLineas);
        } catch (FileNotFoundException fnfe) {
            fnfe.printStackTrace();
        } catch (IOException ioe){
            ioe.printStackTrace();
        } 
        
    }
    
    public void write(String ruta, String texto) throws IOException{
        File fichero = new File(ruta);
       BufferedWriter bw = new BufferedWriter(new FileWriter(fichero));
        PrintWriter pw = new PrintWriter(bw);
	pw.println(texto);
        bw.close();
    }
    public String rutaActual(){
        Path ruta = Paths.get(System.getProperty("user.dir"));
        return ruta.toString();
    }
}