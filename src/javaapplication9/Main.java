
package javaapplication9;

import java.io.IOException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
       public static void main(String[] args) {
   Scanner sc = new Scanner (System.in);
   Terminal t = new Terminal();
   boolean dentro = true;
   System.out.println("Bienvenidos a la consola de comandos de Adrian Herrero, Daniel Hidalgo, Pablo Gil :D");
    while (dentro){
        System.out.println("Inserta un comando");
        String comando = sc.nextLine();
        String direccion = "";
        String texto = "";
        
        switch (comando){
            case "help":
                System.out.println(t.help());
                break;
            case "cd":
                t.cd(direccion = sc.nextLine());
                break;
                
            case "close":
                System.out.println("Hasta pronto!!");
                dentro = t.close();
                break;
            case "clear":
                for (int i = 0; i < 10; i++) {
                    System.out.println(t.clear());
                }
                break;
            case "info":
                t.info();
                break;
            case "top":           
                try {
                    t.top(direccion = sc.nextLine());
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            case "dir":
                t.dir(direccion = sc.nextLine());
                break;
            case "mkdir":
                t.mkdir(direccion = sc.nextLine());
                break;
            case "mkfile":
            {
                try {
                    System.out.println("Escribe el fichero y texto que deseas que sea creado :3");
                    t.mkfile(direccion = sc.nextLine(), texto = sc.nextLine());
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }            
            break;
            case "delete":
                t.delete(direccion = sc.nextLine());
                break;
            case "cat":
            {
                try {
                    t.cat(texto = sc.nextLine());
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                break;
            case "write":
            {
                try {
                    System.out.println("Dame el nombre de un fichero y escribe el texto que desees");
                    t.write(texto = sc.nextLine(), direccion = sc.nextLine());
                } catch (IOException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
                break;

            default:
                System.out.println("No hay ningún comando válido, ejecuta help para revisar las opciones");
                break;
            }
        }               
    }    
}
              